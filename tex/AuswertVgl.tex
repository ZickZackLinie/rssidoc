\chapter{Einbeziehen weiterer Methoden}
\label{chap:auswertvgl}

Das vorherige Kapitel hat gezeigt, welche Positionsbestimmungsmöglichkeiten anhand der vorgestellten Verfahren und Parameter zu erwarten sind. Dabei kann keines der Verfahren ohne konkrete Annahmen zur Umgebungssituation und Anpassung der Algorithmen verwendet werden. Selbst die Verwendung der plausibelsten Lösung kann durch eine schlechte Ankerknotenverteilung im Fall eines Flips zu großen Fehlern führen. In diesem Abschnitt werden Hinweise zur Verbesserung einer Positionsbestimmung durch zusätzliche Verfahren gegeben, die vom Autor dieser Arbeit als vielversprechende Erweiterung gesehen werden.

Dazu werden zunächst drei Ansätze vorgestellt, die in der stationären Positionsbestimmung verwendet werden können, um anschließend die mobile Positionsbestimmung durch Abschätzung der Bewegungsgeschwindigkeit einzubeziehen. Zuletzt werden weitere An"-sä"-tze unter Verwendung zusätzlicher Hardware vorgestellt.

\section{Stationäre Positionsbestimmung}

	Die Methode von Kuo \textit{et\,al.} bestimmt unter Verwendung der Wahrscheinlichkeitsdichtefunktion an vordefinierten, gleichmäßig im Untersuchungsgebiet verteilten Punkten einen Wert, der als Maß der Wahrscheinlichkeit für die Existenz des gesuchten Knotens verwendet wird \cite{kuo2005}. Daraus ergibt sich die Möglichkeit zur Bewertung der Verteilung der verwendeten Ankerknoten. Die ermittelte Wahrscheinlichkeit $G_{P_\mathrm{S}}(P_\mathrm{S}')$ aus Gleichung~\ref{eq:pdf:gi:b}, dass die Position von S auf den Punkt $P_\mathrm{S}'$ geschätzt wird, multipliziert mit der Abweichung der ermittelten Lösung $\mdist(P_\mathrm{S}, P_\mathrm{S}')=D_\mathrm{S}(\mathrm{WDF})$ ergibt nach Gleichung~\ref{eq:pdf:err:a} ein gewichtetes Fehlermaß für die Positionsbestimmung.

	\begin{equation}\label{eq:pdf:err:a}
	\begin{split}
		E_1(\mathrm{S}, \mathrm{S}') &= G_{P_\mathrm{S}}(P_\mathrm{S}') \cdot \mdist(P_\mathrm{S}, P_\mathrm{S}')\\
		&= G_{P_\mathrm{S}}(P_\mathrm{S}') \cdot D_\mathrm{S}(\mathrm{WDF})
	\end{split}
    \end{equation}

	Sei $U$ die Menge der vordefinierten Abtastpositionen der Umgebung, so kann ein zweites Maß $E_2(P_\mathrm{S})$ nach Gleichung~\ref{eq:pdf:err:b} definiert werden. Diese Fehlerfunktion betrachtet alle möglichen Abweichungen. Je enger der Raum mit hohen Wahrscheinlichkeitswerten um S liegt, desto geringer wird der Wert von $E_2$. Sind die Werte durch hohe Standardabweichungen der Messergebnisse breit über die Umgebung verteilt, bewirkt das lineare Einbeziehen der Abweichungsdistanz einen hohen Wert von $E_2$ für $P_\mathrm{S}$.
	
	\begin{equation}\label{eq:pdf:err:b}
	\begin{split}
		E_2(\mathrm{S}) &= \sum_{P_e\in U} E_1(\mathrm{S}, P_e)
	\end{split}
    \end{equation}
	
	Schließlich definieren die Autoren der Arbeit eine Fehlerfunktion $E_\mathrm{ges}$ nach Gleichung~\ref{eq:pdf:err:c} für die allgemeine Auffindbarkeit eines Knotens im gegebenen Netz. Eine Möglichkeit, die Positionsbestimmung der vorgestellten Verfahren innerhalb einer Umgebung zu verbessern, besteht folglich darin, die Ankerknoten derart zu verteilen, dass ein möglichst geringer Fehlerwert $E_\mathrm{ges}$ für das Netz ermittelt wird. Folglich steigen die Chancen eine Position korrekt zu bestimmen.
	
	\begin{equation}\label{eq:pdf:err:c}
	\begin{split}
		E_\mathrm{ges} &= \sum_{P_l\in U} E_2(P_l)\\
		&= \sum_{P_l\in U}\sum_{P_e\in U} E_1(P_l, P_e)
	\end{split}
    \end{equation}
    
	
	Weiterhin kann, analog zu dem in Kapitel~\mref{chap:verwandtearbeiten} vorgestellten System Radar \cite{radar2000}, ein Clipping- und Mustererkennungsalgorithmus verwendet werden. Dabei wird die grafische Darstellung der Umgebung in Form einer Karte oder ähnlichem Material analysiert, um den Verlauf von Straßen und Gebäuden zu erkennen. Für aufeinander folgende Positionsbestimmungen kann die Plausibilität des Ergebnisses untersucht werden, wenn Annahmen über die Umgebung aufgrund der Kartendigitalisierung getroffen werden können. Ergibt die Mustererkennung ein weitläufiges, flaches Gebiet, könnte von weniger Störobjekten ausgegangen werden. Außerdem können räumliche Trennungen der Verbindung zwischen Such- und Ankerknoten erkannt und eine resultierende, zusätzliche Dämpfung bei der Distanzschätzung berücksichtigt werden.\\
	
	In der Arbeit von Kaemarungsi und Krishnamurthy werden bei Signalen von mehr als drei Ankerknoten die Verbindungen zur Positionsbestimmung ausgewählt, deren RSSI-Werte auf minimale Dämpfungen schließen lassen \cite{kaemarungsi2004}. Somit liegt der gesuchte Knoten meist näher am Mittelpunkt des zugehörigen Ankerdreiecks, was zu einer deutlichen Verbesserung der Positionsbestimmung führt.




\section{Mobile Positionsbestimmung}

	In dieser Arbeit wurde nur die stationäre Positionsbestimmung, also für einen unbewegten Knoten S, untersucht. Geht man von einem mobilen Suchknoten (in Bewegung) aus, ergeben sich weitere Möglichkeiten zur Verbesserung der ermittelten Position. Dong und Dargie haben den Einfluss der durchschnittlichen Bewegungsgeschwindigkeit auf die Änderung der Messwerte untersucht \cite{qianDong2012}. Dort wurde unter der Annahme einer Geschwindigkeit die Zeitdauer für eine RSSI-Messung bestimmt, sodass der gesuchte Knoten eine festgelegte Distanz noch nicht überschreiten konnte. Die gemessenen RSSI-Werte konnten in dem Fall mit einer maximalen Distanzabweichung verglichen werden, was zu einer Eingrenzung des Fehlers der Distanzschätzung führte.\\
	
	Ein erweiterter Ansatz wird in der Arbeit von Jiang verwendet \cite{masterarbeit}. Unter der Annahme, dass die mittlere, zurückgelegte Distanz $d_\mathrm{avg}$ bekannt ist, wurden für je zwei Messzeiten durch verschiedene Verfahren je eine Menge möglicher Punkte $P_1$ und $P_2$ ermittelt. Die Distanzen $D_i=\{d\,|\,d=\mdist(p_i, p_{i+1}), p_i\in P_i\}$ zwischen den Punkten der Mengen werden paarweise bestimmt und anschließend ein Shortest-Path-Algorithmus angewendet, der für jede Messzeit eine Lösung $p_i$ ermittelt, sodass die Abweichungen $|d_\mathrm{avg}-d_i|$ von der durchschnittlich zurückgelegten Distanz über alle Messzeit-Mengen $P_i$ minimal wird.
	Es wird damit die an die Bewegungsgeschwindigkeit angepasste, plausibelste Lösung des Bewegungspfades ermittelt. Wird der Knoten kurzzeitig mit konstanter Geschwindigkeit geradlinig durch ein stark gestörtes Gebiet geführt, so kann der resultierende, stark abweichend bestimmte Punkt über die mittlere Bewegung korrigiert werden.\\
	
	Bei der Verwendung seitlich positionierter Referenzknoten zur Distanzkalibrierung fiel auf, dass durch \cquote{Vorbeilaufen} an einem Knoten unter Kenntnis der Signaldämpfung die minimale Entfernung zu dem Knoten bestimmt werden kann. Andersherum kann die Signaldämpfung unter Kenntnis der minimalen Entfernung durch Kurvenschätzung nach Kapitel~\mref{chap:abstand} ermittelt werden.

	\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{bilder/park1_1_4_10perc.png}
	\caption[Distanz: Seitenmessung in mäßig gest. Umgeb. zw. 1 und 4]{Messergebnisse der Kalibrierung vom \datePark{} im mäßig gestörten Umfeld (Park) der Knoten 1 und 4 (Referenzmessung).}
	\label{fig:erg:park1_14}
	\end{figure}
	
	Abbildung~\ref{fig:erg:park1_14} zeigt die Messwerte zwischen den Knoten 1 und 4 der Messung im Park, während Knoten 1 auf der $80\,\mathrm{m}$ langen, primären Messlinie entlang geführt wurde. Dem Aufbau der Messung nach Abschnitt~\mref{sec:aufbau:park}, betrug der Abstand zwischen Knoten 4 und dieser Messlinie mindestens $7\,\mathrm{m}$. Zu den Messwerten sind drei Kurvenverläufe zu sehen, die unter dieser Annahme bei zusätzlich unterschiedlich angenommenen $\gamma$-Werten den theoretischen Kurvenverlauf der Messung darstellen. Da der mittlere Pfadverlustkoeffizient nach Tabelle~\ref{tab:gam:park} auf $\bar{\gamma} = 1,894$ bestimmt wurde, sollte der grün dargestellte Kurvenverlauf am besten zu den Messwerten passen. Die rechte Hälfte der Abbildung zeigt deutlich den bekannten Signaleinbruch zwischen $25\,\mathrm{m}$ und $30\,\mathrm{m}$.
	
	Stellt man einen Messknoten am Seitenrand einer Gasse auf und bewegt einen gesuchten Knoten entlang dieser Gasse am Messknoten vorbei, so ist ein ähnlicher Kurvenverlauf zu erwarten. Da statt den genauen Abständen der Zeitverlauf und die maximale Reichweite der Knoten als bekannt angenommen werden, kann mittels dieser Messungen eine Durchschnittsgeschwindigkeit des Suchknotens und der mittlere Abstand zum Messknoten geschätzt werden. Ähnliche Annahmen werden von Xu \textit{et\,al.} getroffen, um Bewegungsmuster von Passanten mit Mobiltelefonen auf Straßen zu erkennen \cite{xu2013}. 
	

\section{Zusätzliche Hardware und Fremdsysteme}

	Die in dieser Arbeit vorgestellten Ergebnisse basieren auf RSSI-Werten, die mit vertikal ausgerichteten, $360^\circ$ horizontal strahlenden Dipolen gemessen wurden. Diese Antennen haben die Eigenschaft, dass theoretisch bei gleicher Ausrichtung und Höhe der Sendeantenne in konzentrischen Kreisen um die Empfangsantenne immer die gleiche Signalstärke vom Sender gemessen wird. Anders verhalten sich Richtantennen. Diese besitzen aufgrund der Bauart eine erhöhte Empfangssensibilität und Sendeleistung in eine Richtung \cite{moltrechtA2013, seybold2005}. Dreht man die Antenne um die vertikale Achse, kann nach einem Knoten gesucht werden. Wird bei gleichbleibender Sendestärke des gesuchten Knotens und Distanz zu diesem während der Drehung der Richtantenne eine erhöhte Signalstärke erkannt, so liegt der gesuchte Knoten in Richtung der Richtantenne. Auf diese Weise können die Winkel zwischen Knoten bestimmt mittels Triangulation zur Positionsbestimmung verwendet werden. Die Arbeit von Pierlot \textit{et\,al.} beschäftigt sich mit der Triangulationsmethode zur Positionsbestimmung \cite{triangulation}.\\
	
	Gibt es keine Möglichkeit, die Ausrichtung einer Richtantenne zu ändern oder zu über"-prü"-fen, so bietet die in Kapitel~\mref{chap:verwandtearbeiten} vorgestellte Arbeit von Sen \textit{et\,al.} unter Verwendung spezieller WLAN-Router eine Möglichkeit, die Winkel zwischen Knoten ohne Rotation der Antennen zu bestimmen \cite{souvikSen2013}. Dabei werden Router mit drei Dipolantennen und speziellen Chipsätzen verwendet, die es ermöglichen, aus den Phasenverschiebungen der Signale die Ursprungsrichtung eines Signals und Informationen zum Übertragungsmedium zu errechnen. Dämpfungen und Spiegelungen eines Signals durch Störobjekte in der direkten Sichtverbindung können so in die Distanzbestimmung einbezogen werden.
	Zur Positionsbestimmung mittels dieser Geräte ist nur noch ein Ankerpunkt notwendig. Jedoch ist für die Verwendung des Systems eine Anpassung der Routersoftware erforderlich.\\
	
	Die in Kapitel~\mref{chap:verwandtearbeiten} vorgestellten Fingerprinting-Ansätze wurden in dieser Arbeit aufgrund der nötigen Vorleistungen ausgeschlossen. RSSI-Fingerprinting-Methoden verwenden, analog zum WDF-Verfahren, vordefinierte Positionen, an denen in der Vorbereitungsphase die RSSI-Werte aller verfügbaren Signale gemessen werden, um sie später zur Positionsbestimmung mit Werten vergleichen zu können, die am Suchknoten gemessen werden. Wurde eine Datenbank in der Vorbereitungsphase erstellt und mit Daten gefüllt, können diese von verschiedenen Algorithmen weiter verwendet werden. Dadurch ist es möglich, derartige Daten auszutauschen. In der Arbeit von Brouwers und Woehrle \cite{gpsvsgoogle} wird beschrieben, wie die Google Maps Geolocation API \cite{google} zur Positionsbestimmung einbezogen werden kann. Es gibt viele derartige Webdienste, denen man die Namen und ermittelten Signalstärken aller WLAN-Stationen in Reichweite übermittelt. Dieser Dienst gleicht die erhaltenen Daten durch RSSI-Fingerprinting mit den Einträgen der eigenen Datenbank ab und schickt als Antwort die ermittelte Position des gesuchten Knotens zurück, der diese Signale empfangen hat. So ist es für Mobiltelefone möglich, bei eingeschaltetem WLAN-Empfänger und Internetanbindung, die Position bestimmen zu lassen, ohne selbst aufwendige Berechnungen durchführen zu müssen.
	


