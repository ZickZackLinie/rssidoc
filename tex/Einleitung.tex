\chapter{Einleitung}
\label{chap:einleitung}

\section{Motivation}\label{sec:einleitung-motivation}

Aufgrund zunehmender Überwachungsmöglichkeiten von Zielpersonen und Zielgruppen entstand im Jahr 2015 eine Masterarbeit zur Bedrohungsanalyse bezüglich Ortung und Verfolgung von WLAN-Endgeräten, nachfolgend als \textit{Tracking} bezeichnet. Dabei wurde Machbarkeit unter monetären und handwerklichen Gesichtspunkten untersucht \cite{haake}. In diesem Rahmen wurde von Daniel Haake, dem Author der Arbeit, die Software \textit{lokiNET}\, \cite{lokinet} entwickelt, die Kommunikation von WLAN-Endgeräten erkennt und die Teilnehmer in einer Datenbank verzeichnet. Des Weiteren besteht die Möglichkeit, die Software an verschiedenen Orten gleichzeitig zu betreiben, um auf einer Karte die Position der getrackten Geräte und deren zeitliche Veränderung darzustellen.

Ein modellierter Bestandteil der Softwarekriterien war die autarke Energie- und Internetanbindung und das Zusammentragen der Daten durch Einsammeln der Geräte. Weiterhin wurden die getrackten Geräte nur mit der hinterlegten Position des lokiNET betreibenden Gerätes in Verbindung gebracht, nicht jedoch der dazu relative Abstand für eine feiner granulierte Verfolgung der Position betrachtet.


Zu Beginn des Jahres 2016 entstand deshalb die Überlegung zur Fortführung und Erweiterung des Projekts unter anderen Aspekten.
Durch eine vernetzte Lösung soll das aufwendige Einsammeln und manuellen Verorten der Geräte abgeschafft werden.
Außerdem soll die eigene Verortungstechnologie verwendet werden, um bei nur wenigen bekannten Positionen der lokiNET-Geräte, die weiterhin als Knoten bezeichnet werden, die restlichen Positionen hinreichend genau zu bestimmen.
\\

Ziel dieser Arbeit ist es, die Knoten durch ein drahtloses Netzwerk zu verbinden und zu analysieren, wie bei möglichst wenigen verorteten Knoten die restlichen Positionen ermittelt werden können.\\

Während die Vernetzung durch ein geroutetes \textit{Wireless Local Area Network} (WLAN) im Ad-hoc-Betrieb mit geeignetem Protokoll erreicht wird, gibt es bei der Positionsbestimmung eine Vielzahl von Möglichkeiten. Das \textit{Global Positioning System} (GPS) ist eines der ersten Positionssysteme und kann für die Zivilbevölkerung eine Genauigkeit von bis zu $10\,\mathrm{m}$ erreichen \cite{e_gps, gpsvsgoogle}. In Gebäuden und sogar in der Nähe vieler Gebäude wird das Satellitensignal blockiert und gestört, wodurch das System nur auf freien Flächen optimal verwendet werden kann. Andere Systeme wie die Verwendung zellulärer Netzwerke \cite{e_cell}, Ultraschallsysteme \cite{e_usound}, Kameraerkennung \cite{e_camera}, Infrarot-Messungen \cite{e_ir}, Bluetooth-Signalstärken \cite{e_bt} oder das Einbeziehen der Informationen aus dem PHY-Layer \cite{e_phy, souvikSen2013} können für derartige Umgebungen genutzt werden. Sie sind jedoch oft mit extremen Kosten und Aufwand in der Anschaffung, Bereitstellung und Änderung durch Aufnahme neuer Knoten verbunden.\\

Die dabei verwendeten Techniken zur Positionsbestimmung umfassen unter anderem das Auswerten der Ankunftszeit (TOA von \textit{time of arrival}) eines Signals, Laufzeit (TDOA von \textit{time difference of arrival}), Winkel (AOA von \textit{angle of arrival}) und Stärke des eintreffenden Signals (RSS von \textit{received signal strength}).

Seit Einführung des IEEE $802.11$ Industriestandards \cite{ieee80211} verwenden viele Positionssysteme die WLAN-Signalstärke als Maß der Distanz. Jedes Gerät mit einem WLAN-Sender/-Empfänger kann ohne großen Aufwand in die Untersuchung eingebunden werden. Deswegen wird in dieser Arbeit eine Ortung auf Basis der WLAN RSS verwendet. Dabei gibt es drei verwendbare Techniken: die Verwendung des stärksten Senders, Fingerprinting und die Verwendung des Signalverlustmodells (engl. \textit{path loss model}).

Die erste Methode assoziiert mit einem WLAN-Gerät die Position des Senders, dessen Signal das WLAN-Gerät am stärksten empfängt. Die Lösung ist leicht umzusetzen, die Genauigkeit hängt jedoch stark von der Senderabdeckung ab.

Darauf aufbauend wird bei der Fingerprinting-Methode bei $N$ Sendern die Position des Gerätes über die kleinste Euklidische Distanz des Vektors aller sichtbaren Sender im $N$-dimensionalen Raum zu bekannten Vektoren vorher referenzierter Positionen ermittelt. Derartige Vorgehensweisen benötigen viel Zeit und Aufwand bei der Aufnahme neuer Gebiete und Access Points (AP), können aber die Position der WLAN-Geräte schnell und sehr genau bestimmen.

Methoden, die das Verlustmodell von WLAN RSS einbeziehen, verwenden die Ab"-schwä"-chung der Signalstärke über eine Distanz zur Bestimmung der Entfernung eines WLAN-Gerätes zu einem Sender und können bei mindestens drei sichtbaren Sendern die Position über Trilateration ermitteln.\\

In dieser Arbeit werden die Knoten nach dem IEEE $802.11$ Industriestandard vernetzt und anhand verschiedener, auf dem Verlustmodell von WLAN RSS basierter Verfahren untersucht, wie gut die Positionen dieser Knoten in unterschiedlichen Umgebungen ermittelt werden können. Eigenschaften des Netzwerks und die veränderlichen Parameter der Methoden werden dafür einbezogen. 
Es werden dabei Anforderungen an diese Arbeit gestellt, die nachfolgend diskutiert werden. Eine Übersicht der ausgewählten Kriterien befindet sich am Ende des nächsten Abschnitts.
\\


\section{Anforderungen}\label{sec:anforderungen}

Die Vernetzung der Knoten soll eine Erweiterung des bestehenden lokiNET-Systems sein, um aktuelle Trackingwerte ohne Einsammeln der Daten vor Ort zu erhalten und auf aktuelle Geschehnisse entsprechend zu reagieren.
Da die bestehenden Knoten bereits mit einem WLAN-Modul ausgerüstet sind, wird für die Vernetzung ein auf WLAN basiertes Routingprotokoll ausgewählt. Daraus ergibt sich das erste Kriterium als Punkt (1): \cquote{Die Knoten sollen untereinander zum Datenaustausch durch WLAN verbunden werden können.}
\\

Weiterhin sollen nur wenige Knoten ihre Startposition kennen. Diese Knoten werden in den folgenden Abschnitten als Ankerknoten bezeichnet. Für die restlichen Geräte soll \textit{die Position aus den Abständen der Knoten voneinander ermittelt} werden können. Daraus ergibt sich als Kriterium Punkt (3) der unten aufgeführten Übersicht. Dabei werden in dieser Arbeit zwei Ansätze unterschieden. Einerseits werden die Möglichkeiten des gesuchten Knotens betrachtet, um seine Position aus den empfangenen Signalen der Ankerknoten ermitteln zu können. Gegenläufig wird die Methode untersucht, die Ankerknoten Informationen über die Signalstärke des Suchknotens austauschen zu lassen, um dessen Position zu ermitteln. Die Ergebnisse werden gegenüber gestellt.
\\

Zur Bestimmung der Position durch Trilateration müssen vorher die Abstände ermittelt werden. Folglich kann für eine WLAN RSS basierte Methode ein weiteres Kriterium definiert werden. Punkt (2) der unten aufgeführten Kriterien: \cquote{Das Verlustmodell der WLAN-Feldstärke soll als ein geeignetes Maß zur Abstandsschätzung untersucht werden}.
\\

Da in der Arbeit von Daniel Haake monetäre Aspekte eine übergeordnete Stellung einnahmen, besteht das lokiNET aus einer Reihe von kostengünstigen und in der Leistung an die Anforderungen angepassten Kleinstgeräten.
Die Ressourcen derartiger Geräte sind stark eingeschränkt. Folglich ergibt sich daraus ein weiteres Kriterium an die vorgestellte Arbeit. Punkt (4): \cquote{Das bestehende System aus Kleinstgeräten soll zur Umsetzung der verbleibenden Anforderungen nur minimal erweitert/eingeschränkt werden.}
\\

Die vier genannten Anforderungen an das System bilden eine Kombination aus Notwendigkeiten zum Erreichen des Ziels und der Beschränkung auf einen Umfang, der den einer Masterarbeit nicht überschreitet.
\\

%\crand{Anweisungen auch nennen?}
Das Ziel dieser Arbeit ist die Erweiterung der bestehenden lokiNET-Infrastruktur um eine Vernetzung der beteiligten Knoten zum Austausch von Messwerten und Anweisungen. 
Weiterhin beinhaltet die Erweiterung die Verwendung des Verlustmodells zur Schätzung der Distanzen zwischen benachbarten Knoten aus RSS-Informationen, sowie das Einbeziehen dieser geschätzten relativen Abstände zur Positionsbestimmung aller Knoten bei einer gegebenen Menge von Ankerknoten. Die Arbeit wird als Erfolgreich betrachtet, wenn die folgenden vier Kriterien erfüllt werden:

\begin{itemize}
\item[(1)] \textbf{Vernetzung:} \cquote{Die Knoten sollen untereinander zum Datenaustausch durch WLAN verbunden werden können.}
\item[(2)] \textbf{Abstand:} \cquote{Das Verlustmodell der WLAN-Feldstärke soll darauf untersucht werden, ob aus beobachteten WLAN-Feldstärken ein geeignetes Maß zur Abstandsschätzung ermittelt werden kann.}
\item[(3)] \textbf{Position:} \cquote{Die fehlenden Positionen der Knoten sollen durch die ermittelten relativen Abstände und einer gegebenen Menge bekannter Ankerknoten bestimmt werden und die Qualität der Positionsangabe untersucht werden.}
\item[(4)] \textbf{Material:} \cquote{Das bestehende System aus Kleinstgeräten soll nur minimal erweitert oder eingeschränkt werden.}
\end{itemize}

Die Umsetzung der Kriterien wird durch ein Experiment verdeutlicht, in dem eine ausgewählte Konfiguration von Knoten vernetzt und die fehlenden Positionen ermittelt werden.

%\begin{itemize}
%\item auf Haakes Arbeit eingehen \cja
%\item Problem der dezentralen Aufbewahrung \cjein
%\item Problem der Positionsangabe \cja
%\item Problem der begrenzten Ressourcen (nur WLAN) \cja
%\item Ziel nennen \cja
%\item Teilziele/Kriterien definieren
%	\begin{itemize}
%	\item Selektion und Verwendung eines Routingprotokolls (batman-adv) \cnein
%	\item Extrahieren der Distanzindikatoren (RSSI über Netlink) \cjein
%	\item Messstrecken für verschiedene Umgebungen definieren \cnein
%	\item (Parametrisierte Distanzfunktion wählen) \cjein
%	\item Ermittlung und Darstellung fehlender Positionen \cja
%	\end{itemize}
%\end{itemize}



\section{Vorgehensweise}\label{sec:einleitung-vorgehen}

Der strukturelle Aufbau der Arbeit orientiert sich grundlegend an den vier genannten Kriterien. Um in dieser Arbeit alle vier Kriterien zu erfüllen, kann eine logische Trennung der Arbeit in Teilgebiete vorgenommen werden, die aufeinander aufbauen. Kriterium $(4)$ wirkt in einschränkender Funktion bei der Erfüllung der verbleibenden drei Anforderungen mit und muss stets einbezogen werden.
\\

Kriterium $(1)$ fordert die Vernetzung der bestehenden Knoten. Folglich beschäftigt sich der zugehörige Teil der Arbeit mit der Auswahl und Umsetzung einer geeigneten Infrastruktur, die den Datenaustausch zwischen den Knoten ermöglicht. Dabei wird auf die Umgebung und mögliche Ausstattung der eingesetzten Geräte eingegangen und eine Lösung vorgestellt, die möglichst einfach und kostengünstig umgesetzt werden kann, ohne die bestehende Gerätekonfiguration maßgeblich zu verändern. Somit wird die Einhaltung des vierten Kriteriums gewahrt.
\\

Das zweite Teilziel der Arbeit beschäftigt sich mit der Schätzung des Abstands zweier Knoten auf Basis des Verlustmodells für WLAN RSS. % Dafür wird zunächst eine Menge von möglichen Indikatoren zur Entfernungsschätzung betrachtet, die gemäß Punkt $(4)$ im Rahmen der akzeptablen Ressourcen liegen.
Das Vorgehen dabei ist zweigeteilt. Zuerst werden in einer Kalibrierungsphase die Parameter des Verlustmodells durch Gegenüberstellung eines RSS-Indikators zur Distanz zweier Knoten bestimmt. Anschließend wird das Modell validiert und eine Fehlerabschätzung vorgenommen, um eine Aussage darüber treffen zu können, welche Genauigkeit bei der Ermittlung einer Distanz erwartet werden kann.
\\

Aufbauend auf der Abstandsbestimmung befasst sich ein Teil der Arbeit mit der Positionsbestimmung einzelner Knoten unter Angabe von Ankerknoten durch Trilateration. %Dabei werden zunächst verschiedene Graphlayout-Algorithmen auf Kompatibilität zur Problemstellung untersucht und ein Kandidat zur Verwendung ausgewählt.
Dieser Teil umfasst ebenfalls zwei Schritte. Zunächst werden verschiedene Algorithmen zur Positionsbestimmung vorgestellt und anhand von Simulationen die erwarteten Qualitäten diskutiert. Darauf aufbauend wird ein eigener Ansatz vorgestellt und mit den anderen verglichen. Im zweiten Teil wird die Positionsbestimmung auf reale Messwerte angewendet und die Ergebnisse der verschiedenen Ansätze bezüglich der Parametrisierung ausgewertet.
\\

Das Entwickeln der Arbeit in der genannten Reihenfolge gewährleistet sowohl das Einhalten der Kriterien, als auch die Erfüllung der Gesamtanforderung. Das bedeutet, mit nur minimalen Erweiterungen oder Einschränkungen kann das bestehende lokiNET-System vernetzt und eine Abstandsschätzung implementiert werden, die unter Angabe von Ankerknoten die Position jedes Knotens bestimmen und zu Validierungszwecken darstellen kann.
\\

Zum Erreichen der Teilziele wird ein Programm zur Steuerung der Knoten entwickelt, das die Konfiguration der Vernetzung automatisiert vornimmt, den Datenaustausch zwischen den Knoten unter Berücksichtigung der Beeinflussung der Messwerte koordiniert und diesen benutzt, um Kalibrierungsmessungen auszulösen. Der Quellcode ist wie die Dokumentation beigelegt. Da sich diese Arbeit jedoch mit dem theoretischen Konzept beschäftigt, wird hier weder auf die Struktur, noch auf die Bedienung oder genaue Implementierung des Programms eingegangen.

%\
%
%\begin{itemize}
%\item für jedes Kriterium sagen, wie man es lösen möchte
%\item vergleichen mit Zielen
%\end{itemize}



\section{Aufbau der Arbeit}\label{sec:einleitung-aufbau}

Der Aufbau der Arbeit orientiert sich an der Reihenfolge zur Erfüllung der gestellten Teilziele. Kapitel~\mref{chap:verwandtearbeiten} behandelt die Ähnlichkeit zu bereits ver"-öf"-fent"-li"-chten Arbeiten, die sich mit Abstands- und Positionsbestimmung auf Basis von WLAN RSS befassen.
%In Kapitel~\mref{chap:grundlagen} werden Grundlagen dieser Arbeit erläutert und Konventionen getroffen, die für das weitere Vorgehen notwendig sind.
Eine Vernetzungsstrategie für die beteiligten Knoten wird in Kapitel~\mref{chap:vernetzung} ausgewählt und die Umsetzung und Auswirkungen auf die nachfolgenden Abschnitte beschrieben.
Die Distanzbestimmung wird in Kapitel~\mref{chap:abstand} behandelt. Dabei wird der \textit{Received Signal Strength Indikator} (RSSI) als ein Maß für die Distanz auf Basis der WLAN RSS vorgestellt und die softwareseitige Umsetzung zur Erfassung dieser Daten erläutert. Anschließend werden daraufhin Messaufbauten entwickelt und die gemessenen RSSI-Werte mit erwarteten Werten des Verlustmodells verglichen, um die Qualität der Messvorrichtungen zu untersuchen. Die Ergebnisse werden verwendet, um eine Abstandsfunktion zu entwickeln, die Distanzen zwischen Knoten auf Basis des RSSI schätzt.
Kapitel~\mref{chap:position} behandelt die Adaption ausgewählter Algorithmen zur Verortung der beteiligten Knoten unter Eingabe einer Menge von Ankerknoten mit zugehörigen Positionen zur Entwicklung eines eigenen Ansatzes. Diese Algorithmen werden untereinander anhand von Simulationen und realen Messwerten verglichen und die Positionsbestimmungsfehler unter verschiedenen Betrachtungsparametern gegenübergestellt. Darauf aufbauend erfolgt ein Vorschlag zur Positionsbestimmung mittels der vorgestellten Algorithmen in unbekannter Umgebung.
In Kapitel~\mref{chap:auswertvgl} werden die Erkenntnisse der vorherigen Kapitel verwendet, um Positionsbestimmungsmöglichkeiten vorzustellen, die in dieser Arbeit nicht ausführlich untersucht werden konnten und weitere Betrachtungen und die Verwendung zusätzlicher Verfahren benötigen.
% Weiterhin wird eine Möglichkeit zur visuellen Überprüfung der Ausgabe durch Anbindung an einen Kartendienst gegeben.
Nach einer Übersicht der umgesetzten Leistungen in dieser Arbeit wird in Kapitel~\mref{chap:schluss} ein Ausblick auf Veränderungen, Verbesserungen und Erweiterung der Implementierungen gegeben.






